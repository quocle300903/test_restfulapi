<?php

namespace Tests\Feature\Posts;

use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use App\Models\Post;

class GetListPostTest extends TestCase
{
    /** @test */
    public function user_can_get_list_posts()
    {
        $response = $this->getJson(route('posts.index'));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has('status code')
                ->has('data')
                ->has('message')
        );
    }

    /** @test */
    public function user_can_create_post()
    {
        $postData = [
            'name' => 'Test Post',
            'body' => 'Lorem ipsum dolor sit amet.'
        ];

        $response = $this->postJson(route('posts.store'), $postData);

        $response->assertStatus(Response::HTTP_OK)
         ->assertJson([
             'data' => [
                 'name' => $postData['name']
             ]
         ])
         ->assertJsonStructure([
             'data' => [
                 'id',
                 'name',
             ]
         ]);

    }


    /** @test */
    public function user_can_get_single_post()
    {
        $post = Post::factory()->create();

        $response = $this->getJson(route('posts.show', $post->id));

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'data' => [
                    'id' => $post->id,
                    'name' => $post->name,
                ]
            ])
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                ]
            ]);
    }


    /** @test */
    public function user_can_update_post()
    {
        $post = Post::factory()->create();
        $updatedData = [
            'name' => 'Updated Post Name',
            'body' => 'Updated post body.'
        ];

        $response = $this->putJson(route('posts.update', $post->id), $updatedData);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'data' => [
                    'id' => $post->id,
                    'name' => $updatedData['name']
                ]
            ])
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                ]
            ]);
    }


    /** @test */
    public function user_can_delete_post()
    {
        $post = Post::factory()->create();

        $response = $this->deleteJson(route('posts.destroy', $post->id));

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'data' => [
                    'id' => $post->id
                ]
            ]);
    }
}
