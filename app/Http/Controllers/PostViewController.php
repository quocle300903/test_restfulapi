<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostViewController extends Controller
{
    //create a function to get all posts from the database
    public function index()
    {
        $posts = \App\Models\Post::all();
        return view('posts.index', compact('posts'));
    }
}

