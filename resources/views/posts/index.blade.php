@extends('layouts.app')
@section('content-style')
<style>
    table {
        width: 100%;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 8px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }

    th {
        background-color: #f2f2f2;
    }

    tr:hover {
        background-color: #f5f5f5;
    }
</style>
@endsection
@section('content')

<h1>All Posts</h1>
<table>
    <thead>
        <tr>
            <th>Title</th>
            <th>Body</th>
        </tr>
    </thead>
    <tbody>
        @foreach($posts as $post)
        <tr>
            <td>{{ $post->name }}</td>
            <td>{{ $post->body }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
